# Photography - A Beginner's Guide #

Have you ever before recognized how sometimes you've taken a go and which weird color developing in it? 

A yellow, blue, orange look to the photography perhaps? 

Most of us have so when we begin out in our photographic journey it could be a bit of a struggle to understand why this is going on. 

Enter 'White Balance'! 

Different light resources will add a different color cast to your images even though to the naked eyeball they appear 'normal'. Fluorescent light is blue in color actually, tungsten lights add yellow. 

The naked eyeball and the mind behind it are smart enough to discern these dissimilarities and for that reason to us a white newspaper is a white newspaper... is a white newspaper! 

The 'brain' in your camera is not quite that smart and won't normalize the range of color temperatures that we can. 

The White Balance settings in your camera are here to help as this can have a significant impact on the quality of the images that you take as you can take control of your camera and tell it to 'warm up the image' or 'cool down the image'. 

Research the configurations for your individual camera's white balance function. You are able to do this or generally these days personally, have preset white balance adjustments. 

Below are some of the essential white balance settings that most cameras will have: 

Auto- that's where the camera makes a best figure on a shot by shot basis. You'll find it works in many situations but it's worthwhile venturing from it for trickier lighting. 

Tungsten-this setting will cool down the colors in your images generally. 

Fluorescent- this setting will warm up the colors in your photographs generally. 

Daylight/Sunny- will keep the white balance in some sort of 'natural' state. 

Cloudy- this environment generally warms things up an impression more than 'daylight' setting. 

Flash- the flash of your camera can be quite a cool light so in Flash WB mode its warms up your injections a touch. 

Shade- shall warm things up a touch. 

Altering the White Balance physically 

It is possible to get pretty decent shots utilizing the above preset prices. You could however, understand how to do this manually. 

The fundamentals behind modifying things manually will remain the same even though the way you need to do it will change from camera to camera. 

In essence, you will set up a research point for your camera what white/grey happens to be as well as your camera will know that this is white. 

After that you can manually adjust the heat factor up or down appropriately reliant on the conditions under which you are shooting! 

As with everything else in photography this is one of the fundamentals. Once you truly learn how to warm or cool an image, feel free to go nuts on the options to get whatever result you are in fact after. 

After all, just because an image is properly exposed and framed, doesn't make it a correct image! 

My crazy world has included effectively combining all the skills of the musician, photographer, custom made, programmer, and marketing consultancy into one crazy life. If you need a perfect wedding photographer contact with “[Wedding Photographers Pittsburgh](http://annieoneillweddings.com)”